package JDBC;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
//테이블에 데이터 입력, 프라이머리키
public class freewifi_insert {

	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://192.168.23.16:3306/seradatabase", "root", "aeae");
		Statement stmt = conn.createStatement();
		// 파일 불러오기
		File f = new File("C:\\Users\\남세라\\전국무료와이파이표준데이터_DB.txt");
		//버퍼드 리더 
		BufferedReader br = new BufferedReader(new FileReader(f));
		String readtxt;
		if((readtxt=br.readLine())==null) {
			System.out.println("빈 파일입니다.");
			return;
		}
		String[] field_name = readtxt.split("\t");
		
		int LineCnt = 0;
		while((readtxt=br.readLine())!=null) {
			//탭으로 구분
			String[] field = readtxt.split("\t");
			String QueryTxt;
			
			QueryTxt = String.format("insert into freewifi ("
					+"no, " //프라이머리키
					+"inst_place, inst_place_detail, inst_city, inst_country, inst_place_flag,"
					+"service_provider, wifi_ssid, inst_date, place_addr_road, place_addr_land,"
					+"manage_office, manage_office_phone, latitude, longitude, write_date)"
					+"values ("
					+" '%d', '%s', '%s','%s','%s','%s','%s',"
					+"'%s', '%s',"
					+"'%s', '%s', '%s', '%s', '%s', '%s', '%s');",
					//읽어온 문서의 레코드를 필드로 구분하여 입력
					LineCnt+1, field[0], field[1], field[2], field[3], field[4], 
					field[5], field[6], field[7], field[8],
					field[9], field[10], field[11], field[12], field[13], field[14], field[15]);
			
			//설치년월이 공란일 경우 건너뛰기
			if(field[7].length() == 0)
				continue;
			stmt.execute(QueryTxt);
			//System.out.printf("%d번째 항목 Insert OK [%s]\n", LineCnt, QueryTxt);
			
			LineCnt++;
				
		}
		br.close();
		stmt.close();
		conn.close();
	}

}
