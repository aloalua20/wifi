package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//거리계산
public class freewifi_distance {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://192.168.23.16:3306/seradatabase", "root", "aeae");
		Statement stmt = conn.createStatement();
		
		double lat = 37.3860521;
		double lng = 127.1214038;
		
		String QueryTxt;
//		QueryTxt = String.format("select * from freewifi where SQRT( POWER( latitude-%f,2) + POWER(longitude-%f,2) ) = "
//				+ "(select MIN( SQRT( POWER( latitude-%f,2) + POWER(longitude-%f,2) ) ) from freewifi);",lat, lng, lat, lng);
		
//		QueryTxt = "select * from freewifi where service_provider='SKT'";
		QueryTxt = "select * from freewifi where inst_country = '수원시'";

		
		ResultSet rset = stmt.executeQuery(QueryTxt);
		int iCnt = 0;
		while(rset.next() ) {
			System.out.printf("*(%d)****************\n", iCnt++);
			System.out.printf("설치장소명: %s\n", rset.getString(2));
			System.out.printf("서비스제공자명: %s\n", rset.getString(7));
			System.out.printf("설치년월: %s\n", rset.getString(9));
			System.out.printf("도로명주소: %s\n", rset.getString(10));
			
		}	
		stmt.close();
		conn.close();
	}
}
