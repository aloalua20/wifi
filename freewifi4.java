package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
//DATE 형태로 변경
public class freewifi4 {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://192.168.23.16:3306/seradatabase", "root", "aeae");
		Statement stmt = conn.createStatement();

		
		//format 사용 가능
		String sqltxt=String.format("update freewifi SET inst_date = str_to_date(inst_date, '%b-%d')");
		//format 사용 불가
		stmt.execute(sqltxt);

		stmt.close();
		conn.close();

	}

}
