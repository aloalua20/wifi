package JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
//무료Wifi 테이블 생성
public class freewifi_create {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://192.168.23.16:3306/seradatabase", "root", "aeae");
		Statement stmt = conn.createStatement();

		stmt.execute("create table freewifi(" //table 생성
				+ "no int not null primary key," //column0 primary key
				+ "inst_place varchar(50),"  //column1 설치장소명
				+ "inst_place_detail varchar(200)," //column2 설치장소 상세
				+ "inst_city varchar(50),"  //column3 설치도시
				+ "inst_country varchar(50),"  //column4 설치도시 상세
				+ "inst_place_flag varchar(50)," //column5 설치시설
				+ "service_provider varchar(50),"  //column6 서비스 제공자
				+ "wifi_ssid varchar(100),"  //column7 와이파이 SSID
				+ "inst_date varchar(50)," //column8 설치날짜
				+ "place_addr_road varchar(200),"  //column9 소재지(도로명)
				+ "place_addr_land varchar(200),"  //column10 소재지(지번)
				+ "manage_office varchar(50)," //column11 관리기관
				+ "manage_office_phone varchar(50)," //column12 관리기관 전화번호
				+ "latitude double," //column13 위도
				+ "longitude double," //column14 경도
				+ "write_date date"//column15 데이터기준날짜
				+ ") DEFAULT CHARSET=utf8;");

		stmt.close();
		conn.close();
	}
}
